/********************************** (C) COPYRIGHT *******************************
 * File Name          : gattprofile.h
 * Author             : WCH
 * Version            : V1.0
 * Date               : 2018/12/11
 * Description        :
 * Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
 * SPDX-License-Identifier: Apache-2.0
 *******************************************************************************/

#ifndef GATTPROFILE_H
#define GATTPROFILE_H

#ifdef __cplusplus
extern "C" {
#endif

/*********************************************************************
 * INCLUDES
 */

/*********************************************************************
 * CONSTANTS
 */

// Profile Parameters
#define SHUTTERPROFILE_CHAR_CONTROL        0           // RW uint8_t - Profile Characteristic 1 value
#define SHUTTERPROFILE_CHAR_NOTIFY         1           // RW uint8_t - Profile Characteristic 4 value

// Simple Profile Service UUID
#define SHUTTERPROFILE_SERV_UUID     0xF9CCE0D0

// Key Pressed UUID
#define SHUTTERPROFILE_CHAR_CONTROL_UUID    0xF9CCE0D2
#define SHUTTERPROFILE_CHAR_NOTIFY_UUID     0xF9CCE0D1

// Simple Keys Profile Services bit fields
#define SHUTTERPROFILE_SERVICE       0x00000001

// Length of characteristic in bytes ( Default MTU is 23 )
#define SHUTTERPROFILE_CHAR_CONTROL_LEN     0x0C
#define SHUTTERPROFILE_CHAR_NOTIFY_LEN      0x0C

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * Profile Callbacks
 */

// Callback when a characteristic value has changed
typedef void (*shutterProfileChange_t)(uint8_t paramID, uint8_t *pValue, uint16_t len);

typedef struct
{
    shutterProfileChange_t pfnShutterProfileChange; // Called when characteristic value changes
} shutterProfileCBs_t;

/*********************************************************************
 * API FUNCTIONS
 */

/*
 * SimpleProfile_AddService- Initializes the Simple GATT Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 */

extern bStatus_t ShutterProfile_AddService(uint32_t services);

/*
 * SimpleProfile_RegisterAppCBs - Registers the application callback function.
 *                    Only call this function once.
 *
 *    appCallbacks - pointer to application callbacks.
 */
extern bStatus_t ShutterProfile_RegisterAppCBs(shutterProfileCBs_t *appCallbacks);

/*
 * SimpleProfile_SetParameter - Set a Simple GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    len - length of data to right
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 */
extern bStatus_t ShutterProfile_SetParameter(uint8_t param, uint8_t len, void *value);

/*
 * SimpleProfile_GetParameter - Get a Simple GATT Profile parameter.
 *
 *    param - Profile parameter ID
 *    value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16_t will be cast to
 *          uint16_t pointer).
 */
extern bStatus_t ShutterProfile_GetParameter(uint8_t param, void *value);

/*
 * shutterProfile_Notify - Send notification.
 *
 *    connHandle - connect handle
 *    pNoti - pointer to structure to notify.
 */
extern bStatus_t shutterProfile_Notify(uint16_t connHandle, attHandleValueNoti_t *pNoti);

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
