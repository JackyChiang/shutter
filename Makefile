######################################
# target
######################################
TARGET = Shutter

#######################################
# paths
#######################################
# Build path
BUILD_DIR = build

######################################
# source
######################################
# C sources
C_SOURCES = \
APP/main.c \
APP/peripheral.c \
HAL/OLED.c \
HAL/FLASH.c \
HAL/MCU.c \
HAL/RTC.c \
HAL/SHUTTER.c \
HAL/SLEEP.c \
HAL/KEY.c \
HAL/MENU.c \
Profile/gattprofile.c \
RVMSIS/core_riscv.c \
StdPeriphDriver/CH58x_adc.c \
StdPeriphDriver/CH58x_clk.c \
StdPeriphDriver/CH58x_flash.c \
StdPeriphDriver/CH58x_gpio.c \
StdPeriphDriver/CH58x_pwr.c \
StdPeriphDriver/CH58x_spi0.c \
StdPeriphDriver/CH58x_sys.c \
StdPeriphDriver/CH58x_uart1.c \

# C includes
C_INCLUDES = \
-IAPP/inc \
-IStdPeriphDriver/inc \
-IRVMSIS \
-IHAL/inc \
-IProfile/inc \
-ILIB \

# ASM sources
ASM_SOURCES = \
Startup/startup_CH583.s

#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT = Ld/Link.ld

# libraries
LIBS = -lISP583 -lCH58xBLE
LIBDIR = \
-LLIB \
-LStdPeriphDriver

#######################################
# binaries
#######################################
PREFIX = riscv-none-embed-

CC = $(PREFIX)gcc
#AS = $(PREFIX)gcc -x assembler-with-cpp
CPP = $(PREFIX)g++
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size

HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S


#######################################
# CFLAGS
#######################################
# compile gcc flags
CFLAGS = -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -g -DDEBUG=1 -DBLE_MAC=0 $(C_INCLUDES) -std=gnu99 -w
ASFLAGS = -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -g -x assembler
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
ASFLAGS += -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
 
LDFLAGS = -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -g -T
LDFLAGS += $(LDSCRIPT) -nostartfiles -Xlinker --gc-sections $(LIBDIR) -Xlinker --print-memory-usage -Wl,-Map=$(BUILD_DIR)/$(TARGET).map --specs=nano.specs --specs=nosys.specs -o $(BUILD_DIR)/$(TARGET).elf

#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))

# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

# all #
all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin

$(BUILD_DIR):
	mkdir $@
 
$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR) 
	$(CC) $(CFLAGS)
 
$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(CC) $(ASFLAGS)
 
$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile
	$(CC) $(OBJECTS) $(LDFLAGS) $(LIBS)
	$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $< $@
	
$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $< $@

#######################################
# clean up
#######################################
clean:
	-rm -Rf $(BUILD_DIR)/*.d $(BUILD_DIR)/*.o $(BUILD_DIR)/*.map *.d

cleanall:
	-rm -Rf $(BUILD_DIR)
