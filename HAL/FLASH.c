/*
 * FLASH.c
 *
 *  Created on: June 28, 2022
 *      Author: Administrator
 */

#include "HAL.h"

uint32_t Config_Load(uint8_t config[])
{
    EEPROM_READ(0, config, sizeof(shutterCommand_t));
    return 0;
}

uint32_t Config_Save(uint8_t config[])
{
    EEPROM_ERASE(0, EEPROM_PAGE_SIZE);
    EEPROM_WRITE(0, config, sizeof(shutterCommand_t));
    return 0;
}
