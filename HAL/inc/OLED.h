#ifndef __OLED_H
#define __OLED_H
#include "stdlib.h"
#include "PIN_CONFIG.h"

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//#define OLED_DC_PIN     GPIO_Pin_15
//#define OLED_RES_PIN    GPIO_Pin_5
//#define OLED_MOSI       GPIO_Pin_14
//#define OLED_SCK        GPIO_Pin_13

typedef UINT8 u8;
typedef UINT32 u32;

//OLED控制用函数
void OLED_WR_Byte(u8 dat, u8 cmd);
void OLED_Set_Pos(unsigned char x, unsigned char y);
void OLED_ColorTurn(u8 i); //0正常显示 1反色显示
void OLED_DisplayTurn(u8 i);//0正常显示 1翻转180度显示
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_Refresh_Gram(void);
void OLED_Init(void);
void OLED_Clear(void);
void OLED_Clear_Ram(void);
void OLED_DrawPoint(u8 x, u8 y, u8 t);
void OLED_Fill(u8 x1, u8 y1, u8 x2, u8 y2, u8 dot);
void OLED_ShowChar(u8 x, u8 y, u8 chr, u8 size, u8 mode);
void OLED_ShowNum(u8 x, u8 y, u32 num, u8 len, u8 size, u8 mode);
void OLED_ShowString(u8 x, u8 y, const u8 *p, u8 size, u8 mode);
void OLED_ShowChinese(u8 x,u8 y,u8 index,u8 size, u8 mode);
void OLED_ShowText(u8 x, u8 y, char *str, u8 size, u8 mode);
#endif  

