/*
 * FLASH.h
 *
 *  Created on: June 28, 2022
 *      Author: Administrator
 */

#ifndef INCLUDE_FLASH_H_
#define INCLUDE_FLASH_H_


uint32_t Config_Load(uint8_t config[]);

uint32_t Config_Save(uint8_t config[]);


#endif /* INCLUDE_FLASH_H_ */
