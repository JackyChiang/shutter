/********************************** (C) COPYRIGHT *******************************
 * File Name          : LED.h
 * Author             : WCH
 * Version            : V1.0
 * Date               : 2016/04/12
 * Description        :
 * Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
 * SPDX-License-Identifier: Apache-2.0
 *******************************************************************************/

/******************************************************************************/
#ifndef __SHUTTER_H
#define __SHUTTER_H
#include "PIN_CONFIG.h"


#ifdef __cplusplus
extern "C" {
#endif

#define HAL_CAMERA_SHUTTER_OFF_EVENT        0x0001
#define HAL_CAMERA_POWER_ON_EVENT           0x0002
#define HAL_CAMERA_POWER_OFF_EVENT          0x0004
#define HAL_CAMERA_TIMELAPSE_EVENT          0x0008

#define HAL_KEY_EVENT                       0x0010
#define HAL_OLED_REFRESH_EVENT              0x0020

#define COMMAND_NC              0x00
#define COMMAND_POWER           0x01
#define COMMAND_FOCUS           0x02
#define COMMAND_UNFOCUS         0x03
#define COMMAND_SHUTTER         0x04
#define COMMAND_TIMELAPSE       0x05
#define COMMAND_UNTIMELAPSE     0x06
#define COMMAND_RET_SUCCESS     0x00
#define COMMAND_RET_FAILURE     0xFF
#define COMMAND_RET_INFO        0x06


typedef struct{
    uint16_t    start;
    uint8_t     cmd;
    uint8_t     en;
    uint16_t    total;
    uint16_t    interval;
    uint16_t    na;
    uint16_t    end;
} shutterCommand_t;

void HAL_ShutterInit(void);

void HAL_SPIInit(void);

void HAL_CameraShutterPress();

void HAL_CameraShutterHold();

void HAL_CameraShutterRelease(uint16_t interval);

void HAL_CameraPowerTogger();

void HAL_CameraTimelapse(uint16_t total,uint16_t interval);

#ifdef __cplusplus
}
#endif

#endif
