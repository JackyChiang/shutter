/*
 * KEY.h
 *
 *  Created on: Sep 22, 2022
 *      Author: Administrator
 */

#ifndef _KEY_H_
#define _KEY_H_
#include "PIN_CONFIG.h"

/********************************************************************************
 *                                     MACROS
 *******************************************************************************/
/* Switches (keys) */
#define HAL_KEY_SW_UP        0x01  // key up
#define HAL_KEY_SW_DOWN      0x02  // key down
#define HAL_KEY_SW_ENTER     0x04  // key enter

 /* 按键定义 */

/* 1 - KEY */  //key3-up-PA4  key2-down-PA12  key1-enter-PB15  上拉输入
#define KEY_UP_BV       BV(4)
#define KEY_DOWN_BV     BV(12)
#define KEY_ENTER_BV    BV(15)

#define KEY_UP_PU       (R32_PA_PU |= KEY_UP_BV)
#define KEY_DOWN_PU     (R32_PA_PU |= KEY_DOWN_BV)
#define KEY_ENTER_PU    (R32_PB_PU |= KEY_ENTER_BV)

#define KEY_UP_DIR      (R32_PA_DIR &= ~KEY_UP_BV)
#define KEY_DOWN_DIR    (R32_PA_DIR &= ~KEY_DOWN_BV)
#define KEY_ENTER_DIR   (R32_PB_DIR &= ~KEY_ENTER_BV)

#define KEY_UP_IN       (ACTIVE_LOW(R32_PA_PIN & KEY_UP_BV))
#define KEY_DOWN_IN     (ACTIVE_LOW(R32_PA_PIN & KEY_DOWN_BV))
#define KEY_ENTER_IN    (ACTIVE_LOW(R32_PB_PIN & KEY_ENTER_BV))

#define HAL_PUSH_BUTTON_UP()    (KEY_UP_IN) //添加自定义按键
#define HAL_PUSH_BUTTON_DOWN()  (KEY_DOWN_IN)
#define HAL_PUSH_BUTTON_ENTER() (KEY_ENTER_IN)
/*
 * Initialize the Key Service
 */
void HAL_KeyInit( void );

/*
 * This is for internal used by hal_driver
 */
uint8 HAL_KeyPoll(void);

#endif /* INC_KEY_H_ */
