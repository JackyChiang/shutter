/*
 * PIN_CONFIG.h
 *
 *  Created on: Sep 26, 2022
 *      Author: Administrator
 */

#ifndef _PIN_CONFIG_H_
#define _PIN_CONFIG_H_

#define CAMERA_POWER    GPIO_Pin_11 //PB11  power
#define CAMERA_SHUTTER  GPIO_Pin_12 //PB12  shutter
#define CAMERA_FOCUS    GPIO_Pin_13 //PB13  ok
#define CAMERA_SELECT   GPIO_Pin_14 //PB14  ok

#define OLED_DC_PIN     GPIO_Pin_15 //PA15
#define OLED_RES_PIN    GPIO_Pin_5  //PA5
#define OLED_MOSI       GPIO_Pin_14 //PA14
#define OLED_SCK        GPIO_Pin_13 //PA13

#endif /* INC_PIN_CONFIG_H_ */
