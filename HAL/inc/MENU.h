#ifndef _MENU_H_
#define _MENU_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    uint8_t current_index;
    uint8_t down_index;
    uint8_t up_index;
    uint8_t enter_index;
    uint8_t back_index;
    void (*pFunction)();
} menu_t;

void mainUI(void);
void menuShutter(void);
void menuBulb(void);
void menuTimelapse(void);
void menuProtocol(void);
void menuExControl(void);
void menuExit(void);

void menuShutterDelayTime(void);
void menuShutterStart(void);
void menuShutterExit(void);

void menuBulbTime(void);
void menuBulbStart(void);
void menuBulbExit(void);

void menuTimelapseTotal(void);
void menuTimelapseDelay(void);
void menuTimelapseStart(void);
void menuTimelapseExit(void);

void menuBootloader(void);


#ifdef __cplusplus
}
#endif

#endif
