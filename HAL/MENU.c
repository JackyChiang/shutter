#include "HAL.h"

menu_t const menu_table[19] =
{
        //当前, 下, 上, 确定, 返回
        {0,0,0,1,0,(*mainUI)},
        {1,2,6,7,0,(*menuShutter)},
        {2,3,1,10,0,(*menuBulb)},
        {3,4,2,13,0,(*menuTimelapse)},
        {4,5,3,17,0,(*menuProtocol)},
        {5,6,4,18,0,(*menuExControl)},
        {6,1,5,0,0,(*menuExit)},

        {7,8,9,7,1,(*menuShutterDelayTime)},
        {8,9,7,7,1,(*menuShutterStart)},
        {9,7,8,7,1,(*menuShutterExit)},

        {10,11,12,7,2,(*menuBulbTime)},
        {11,12,10,7,2,(*menuBulbStart)},
        {12,10,11,7,2,(*menuBulbExit)},

        {13,14,16,7,3,(*menuTimelapseTotal)},
        {14,15,13,7,3,(*menuTimelapseDelay)},
        {15,16,14,7,3,(*menuTimelapseStart)},
        {16,13,15,7,3,(*menuTimelapseExit)},

        {17,17,17,17,4,(*menuProtocol)},
        {18,18,18,18,5,(*menuBootloader)}
};

void mainUI(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 7, "星火计划蓝牙快门", 16, 1);
}
void menuShutter(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  ">> 快门", 16, 0);
    OLED_ShowText(0, 16, "   Bulb", 16, 1);
}
void menuBulb(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   快门", 16, 1);
    OLED_ShowText(0, 16, ">> Bulb", 16, 0);
}
void menuTimelapse(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   Bulb", 16, 1);
    OLED_ShowText(0, 16, ">> 延时摄影", 16, 0);
}
void menuProtocol(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   延时摄影", 16, 1);
    OLED_ShowText(0, 16, ">> 协议选择", 16, 0);
}
void menuExControl(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   协议选择", 16, 1);
    OLED_ShowText(0, 16, ">> 固件更新", 16, 0);
}

void menuExit(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   固件更新", 16, 1);
    OLED_ShowText(0, 16, ">> 退出", 16, 0);
}

void menuShutterDelayTime(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  ">> 延迟时间", 16, 0);
    OLED_ShowText(0, 16, "   开始执行", 16, 1);
}
void menuShutterStart(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   延迟时间", 16, 1);
    OLED_ShowText(0, 16, ">> 开始执行", 16, 0);
}
void menuShutterExit(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   开始执行", 16, 1);
    OLED_ShowText(0, 16, ">> 退出", 16, 0);
}

void menuBulbTime(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  ">> Bulb时间", 16, 0);
    OLED_ShowText(0, 16, "   开始执行", 16, 1);
}
void menuBulbStart(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   Bulb时间", 16, 1);
    OLED_ShowText(0, 16, ">> 开始执行", 16, 0);
}
void menuBulbExit(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   开始执行", 16, 1);
    OLED_ShowText(0, 16, ">> 退出", 16, 0);
}
void menuTimelapseTotal(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  ">> 拍摄张数", 16, 0);
    OLED_ShowText(0, 16, "   拍摄间隔", 16, 1);
}
void menuTimelapseDelay(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   拍摄张数", 16, 1);
    OLED_ShowText(0, 16, ">> 拍摄间隔", 16, 0);
}
void menuTimelapseStart(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   拍摄间隔", 16, 1);
    OLED_ShowText(0, 16, ">> 开始执行", 16, 0);
}
void menuTimelapseExit(void)
{
    OLED_Clear_Ram();
    OLED_ShowText(0, 0,  "   开始执行", 16, 1);
    OLED_ShowText(0, 16, ">> 退出", 16, 0);
}

__HIGH_CODE
void menuBootloader(void)
{
    while(FLASH_ROM_ERASE(0,EEPROM_BLOCK_SIZE))
    {
        ;//ROM 擦4K1个单位，擦0地址起始
    }
    FLASH_ROM_SW_RESET();
    R8_SAFE_ACCESS_SIG = SAFE_ACCESS_SIG1;
    R8_SAFE_ACCESS_SIG = SAFE_ACCESS_SIG2;
    SAFEOPERATE;
    R16_INT32K_TUNE = 0xFFFF;
    R8_RST_WDOG_CTRL |= RB_SOFTWARE_RESET;
    R8_SAFE_ACCESS_SIG = 0;//进入后执行复位，复位类型为上电复位
}
