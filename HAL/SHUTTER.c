/* 头文件包含 */
#include "HAL.h"
#include "gattprofile.h"

extern tmosTaskID halTaskId;
extern UINT8 ResponseData[SHUTTERPROFILE_CHAR_NOTIFY_LEN];
void HAL_ShutterInit(void) 
{
    GPIOB_ResetBits(CAMERA_POWER | CAMERA_FOCUS | CAMERA_SHUTTER);
    GPIOB_SetBits(CAMERA_SELECT);
    GPIOB_ModeCfg(CAMERA_POWER | CAMERA_FOCUS | CAMERA_SHUTTER | CAMERA_SELECT,
            GPIO_ModeOut_PP_5mA);
}

void HAL_SPIInit(void) 
{
    //OLED_RESET
    GPIOA_SetBits(OLED_RES_PIN);
    //OLED_D/C#
    GPIOA_SetBits(OLED_DC_PIN);
    GPIOA_ModeCfg(OLED_DC_PIN | OLED_SCK | OLED_MOSI | OLED_RES_PIN,
            GPIO_ModeOut_PP_5mA);
    SPI0_MasterDefInit();
}

void HAL_CameraShutterPress() 
{
    GPIOB_SetBits(CAMERA_FOCUS);
    DelayMs(10);
    GPIOB_SetBits(CAMERA_SHUTTER);
    DelayMs(450);
    GPIOB_ResetBits(CAMERA_SHUTTER);
    DelayMs(1);
    GPIOB_ResetBits(CAMERA_FOCUS);
}

void HAL_CameraShutterHold() 
{
    GPIOB_SetBits(CAMERA_FOCUS);
    DelayMs(10);
    GPIOB_SetBits(CAMERA_SHUTTER);
}

void HAL_CameraShutterRelease(uint16_t interval) 
{
    tmos_start_task(halTaskId, HAL_CAMERA_SHUTTER_OFF_EVENT, MS1_TO_SYSTEM_TIME(interval*1000));
}

void HAL_CameraTimelapse(uint16_t total, uint16_t interval)
{
    total--;
    ResponseData[4]=HI_UINT16(total);
    ResponseData[5]=LO_UINT16(total);
    tmos_set_event(halTaskId, HAL_CAMERA_TIMELAPSE_EVENT);
    tmos_start_task(halTaskId, HAL_CAMERA_TIMELAPSE_EVENT, MS1_TO_SYSTEM_TIME(interval *  1000));
}